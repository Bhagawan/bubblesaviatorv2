package com.example.bubblesaviatorv2.util

import androidx.annotation.Keep

@Keep
data class BubblesAviatorV2SplashResponse(val url : String)