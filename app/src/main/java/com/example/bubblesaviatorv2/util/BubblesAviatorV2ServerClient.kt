package com.example.bubblesaviatorv2.util

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface BubblesAviatorV2ServerClient {

    @FormUrlEncoded
    @POST("BubblesAviatorV2/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<BubblesAviatorV2SplashResponse>

    companion object {
        fun create() : BubblesAviatorV2ServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(BubblesAviatorV2ServerClient::class.java)
        }
    }

}