package com.example.bubblesaviatorv2.util

import android.graphics.Color
import android.graphics.LightingColorFilter

enum class ColorFilters(val filter: LightingColorFilter) {
    GREEN(LightingColorFilter(Color.parseColor("#4A8339"), 1)),
    RED(LightingColorFilter(Color.parseColor("#852D2D"), 1)),
    WHITE(LightingColorFilter(Color.WHITE, 1))
}