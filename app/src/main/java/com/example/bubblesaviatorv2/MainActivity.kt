package com.example.bubblesaviatorv2

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.bubblesaviatorv2.databinding.ActivityMainBinding
import com.example.bubblesaviatorv2.databinding.PopupGameFinishBinding
import com.example.bubblesaviatorv2.databinding.PopupIntroBinding
import com.example.bubblesaviatorv2.game.BubblesView
import com.example.bubblesaviatorv2.game.GameState
import com.example.bubblesaviatorv2.game.GameViewModel
import com.example.bubblesaviatorv2.game.PlaneView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var gameViewModel: GameViewModel
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        gameViewModel = ViewModelProvider(this)[GameViewModel::class.java]
        setContentView(binding.root)

        binding.planeView.setInterface(object : PlaneView.PLaneInterface {
            override fun onCrash() {
                gameViewModel.planeCrash()
            }

            override fun updateDistance(distance: Int) {
                gameViewModel.setDistance(distance)
            }
        })

        binding.bubblesView.setInterface(object : BubblesView.BubblesInterface {
            override fun onFail() {
                gameViewModel.bubblesFail()
            }

            override fun onSuccess() {
                gameViewModel.bubblesSuccess()
            }
        })

        lifecycleScope.launch {
            gameViewModel.planeRestart.collect { if(it) binding.planeView.restart()}
        }
        lifecycleScope.launch {
            gameViewModel.bubblesHide.collect { if(it) binding.bubblesView.hide()}
        }
        lifecycleScope.launch {
            gameViewModel.planeUp.collect { if(it) binding.planeView.pushPlane() }
        }

        lifecycleScope.launch {
            gameViewModel.score.collect { binding.textDistance.text = it }
        }

        lifecycleScope.launch {
            gameViewModel.bubblesTrigger.collect { binding.bubblesView.restart() }
        }

        binding.root.post {
            lifecycleScope.launch {
                gameViewModel.gameIntro.collect { if(it) showIntro() }
            }
            lifecycleScope.launch {
                gameViewModel.gameEnd.collect { if(it) showFinishPopup(gameViewModel.score.value) }
            }
        }
        loadBackground()
    }

    override fun onResume() {
        val gameState = gameViewModel.getGameState()
        if(gameState != null) {
            binding.planeView.setState(gameState.planeState)
            binding.bubblesView.setState(gameState.bubblesState)
        }
        super.onResume()
    }

    override fun onPause() {
        gameViewModel.setGameState(GameState(binding.planeView.getState(), binding.bubblesView.getState()))
        super.onPause()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showIntro() {
        val popupBinding: PopupIntroBinding = PopupIntroBinding.inflate(layoutInflater, binding.root,false)

        val width = (binding.root.width / 1.3f).toInt()
        val height = binding.root.height / 2

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        popupBinding.root.setOnClickListener {
            popupWindow.dismiss()
        }

        popupWindow.setOnDismissListener {
            gameViewModel.restart()
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true

        popupWindow.animationStyle = R.style.PopupAnimation

        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showFinishPopup(distance: String) {
        val popupBinding: PopupGameFinishBinding = PopupGameFinishBinding.inflate(layoutInflater, binding.root,false)

        val width = (binding.root.width / 1.3f).toInt()
        val height = if(binding.root.width > binding.root.height) (binding.root.height * 0.7f).toInt() else (binding.root.height * 0.5f).toInt()

        popupBinding.textFinishDistance.text = distance
        val popupWindow = PopupWindow(popupBinding.root, width, height, true)

        popupBinding.root.setOnClickListener {
            popupWindow.dismiss()
        }

        popupWindow.setOnDismissListener {
            gameViewModel.restart()
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom) {
                bitmap?.let {
                    binding.layoutMain.background = BitmapDrawable(resources, it)
                }
            }
            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/BubblesAviatorV2/back.png").into(target)
    }
}