package com.example.bubblesaviatorv2

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.telephony.TelephonyManager
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebChromeClient
import android.widget.EditText
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.bubblesaviatorv2.databinding.ActivityBubblesaviatorv2SplashBinding
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

@SuppressLint("CustomSplashScreen")
class BubblesAviatorV2SplashActivity: AppCompatActivity() {
    private lateinit var binding: ActivityBubblesaviatorv2SplashBinding
    private lateinit var viewModel: BubblesAviatorV2SplashViewModel

    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBubblesaviatorv2SplashBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this)[BubblesAviatorV2SplashViewModel::class.java]

        initialiseWebView()
        setContentView(binding.root)

        lifecycleScope.launch{
            viewModel.logoVisibility.collect {
                if(it) showLogo()
                else hideLogo()
            }
        }
        lifecycleScope.launch{
            viewModel.showUrl.collect{ if(it.isNotEmpty()) binding.webViewSplash.loadUrl(it) }
        }
        lifecycleScope.launch{
            viewModel.switchToMain.collect{ if(it) switchToMain() }
        }

        if(savedInstanceState == null) viewModel.init((getSystemService(TELEPHONY_SERVICE) as TelephonyManager).simCountryIso, getId())

        onBackPressedDispatcher.addCallback(this , object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if(binding.webViewSplash.onBackPressed()) finish()
            }
        })
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun onResume() {
        super.onResume()
        binding.webViewSplash.onResume()
    }

    override fun onPause() {
        binding.webViewSplash.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.webViewSplash.onDestroy()
    }

    private suspend fun switchToMain()  = withContext(Dispatchers.Main){
        val i = Intent(applicationContext, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(i)
        finish()
    }

    private fun showLogo() {
        binding.layoutSplashLogo.visibility = View.VISIBLE
        binding.webViewSplash.visibility = View.GONE
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: LoadedFrom) {
                binding.imageViewSplashLogo.setImageBitmap(bitmap)
            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/BubblesAviatorV2/logo.png").into(target)
    }

    private fun hideLogo() {
        binding.layoutSplashLogo.visibility = View.GONE
        binding.webViewSplash.visibility = View.VISIBLE
    }

    private fun initialiseWebView() {
        binding.webViewSplash.setMixedContentAllowed(true)
        binding.webViewSplash.webChromeClient = object: WebChromeClient() {
            private var h = 0
            private var uiVisibility = 0
            private var d: View? = null
            private var a: CustomViewCallback? = null
            override fun getDefaultVideoPoster(): Bitmap? = if(d == null) null else BitmapFactory.decodeResource(resources, R.drawable.ic_baseline_video)
            override fun onHideCustomView() {
                (window.decorView as FrameLayout).removeView(d)
                d = null
                window.decorView.systemUiVisibility = uiVisibility
                requestedOrientation = h
                a!!.onCustomViewHidden()
                a = null
            }

            override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
                if(d != null) {
                    onHideCustomView()
                    return
                }
                d = view
                uiVisibility = window.decorView.systemUiVisibility
                h = requestedOrientation
                a = callback
                (window.decorView as FrameLayout).addView(d, FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
                WindowInsetsControllerCompat(window, window.decorView).let { controller ->
                    controller.hide(WindowInsetsCompat.Type.systemBars())
                    controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
                }
            }
        }
        binding.webViewSplash.settings.userAgentString = binding.webViewSplash.settings.userAgentString.replace("; wv", "")
    }

    private fun getId() : String {
        val shP = getSharedPreferences("AppData", Context.MODE_PRIVATE)
        var id = shP.getString("id", "default") ?: "default"
        if(id == "default") {
            id = UUID.randomUUID().toString()
            shP.edit().putString("id", id).apply()
        }
        return id
    }

}