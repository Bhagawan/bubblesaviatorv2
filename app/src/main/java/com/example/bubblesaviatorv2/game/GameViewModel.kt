package com.example.bubblesaviatorv2.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class GameViewModel: ViewModel() {
    private val _planeRestart = MutableSharedFlow<Boolean>()
    val planeRestart: SharedFlow<Boolean> = _planeRestart

    private val _gameIntro = MutableStateFlow(true)
    val gameIntro: StateFlow<Boolean> = _gameIntro

    private val _gameEnd = MutableSharedFlow<Boolean>(1)
    val gameEnd: SharedFlow<Boolean> = _gameEnd

    private val _planeUp = MutableSharedFlow<Boolean>()
    val planeUp: SharedFlow<Boolean> = _planeUp

    private val _score = MutableStateFlow("0")
    val score: StateFlow<String> = _score

    private val _bubblesHide = MutableSharedFlow<Boolean>()
    val bubblesHide : SharedFlow<Boolean> = _bubblesHide

    private val _bubblesTrigger = MutableSharedFlow<Boolean>()
    val bubblesTrigger : SharedFlow<Boolean> = _bubblesTrigger
    private var bubblesTimer : Job? = null

    private var gameState: GameState? = null

    init {
        viewModelScope.launch {
            _score.emit("0")
        }
    }

    fun setDistance(distance: Int) {
        viewModelScope.launch{
            _score.emit(distance.toString())
        }
    }

    fun restart() {
        bubblesTimer?.let {
            if(it.isActive) it.cancel()
        }
        viewModelScope.launch {
            _planeRestart.emit(true)
            _bubblesTrigger.emit(true)
            _gameEnd.emit(false)
            if (gameIntro.value) _gameIntro.emit(false)
        }
    }

    fun bubblesSuccess() {
        viewModelScope.launch {
            _planeUp.emit(true)
        }
        startTimer()
    }

    fun bubblesFail() {
        startTimer()
    }

    fun planeCrash() {
        bubblesTimer?.let {
            if(it.isActive) it.cancel()
        }
        viewModelScope.launch {
            _gameEnd.emit(true)
            _bubblesHide.emit(true)
        }
    }

    fun setGameState(gameState: GameState) {
        this.gameState = gameState
    }

    fun getGameState() : GameState? = gameState

    private fun startTimer() {
        bubblesTimer?.let {
            if(it.isActive) it.cancel()
        }
        bubblesTimer = viewModelScope.launch {
            delay(3000)
            _bubblesTrigger.emit(true)
        }
    }
}