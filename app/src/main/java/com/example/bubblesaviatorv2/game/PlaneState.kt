package com.example.bubblesaviatorv2.game

import androidx.annotation.Keep

@Keep
data class PlaneState(val planeX : Float, val planeHeight: Float, val planeAngle: Float, val dX: Float, val dY: Float,val up: Boolean, val state: Int, val planeInterface: PlaneView.PLaneInterface?)
