package com.example.bubblesaviatorv2.game

import com.example.bubblesaviatorv2.util.ColorFilters

data class Bubble(var x: Int, var y: Int, var size : Float, var moveVectorX : Float, var moveVectorY : Float, val number: Int, var visible : Boolean, var colorFilter: ColorFilters, var state : Boolean) {
    companion object {
        const val NORMAL = true
        const val PRESSED = false
    }
}