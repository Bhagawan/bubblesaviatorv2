package com.example.bubblesaviatorv2.game

data class GameState(val planeState: PlaneState, val bubblesState: BubblesState)
