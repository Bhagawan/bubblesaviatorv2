package com.example.bubblesaviatorv2.game

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.example.bubblesaviatorv2.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*

class PlaneView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeAngle = 0.0f

    private var dX = 0.0f
    private var dY = 0.0f
    private var up = true

    private val plane = BitmapFactory.decodeResource(context.resources, R.drawable.plane)

    private var state = PAUSE

    private var pInterface: PLaneInterface? = null

    companion object {
        const val PAUSE = 0
        const val FLYING = 1
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            if(planeY == 0.0f) planeY = mHeight / 3.0f
            else planeY += mHeight
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == FLYING) updatePlane()
            drawPlane(it)
            drawGround(it)
        }
    }

    /// Public

    fun setInterface(i: PLaneInterface) {
        pInterface = i
    }

    fun restart() {
        state = FLYING
        planeX = 0.0f
        planeY = mHeight / 3.0f
        planeAngle = 0.0f
        dX = 1.0f
        dY = 0.0f
    }

    fun pushPlane() {
        up = true
    }

    fun getState(): PlaneState = PlaneState(planeX, mHeight - planeY, planeAngle, dX, dY, up, state, pInterface)

    fun setState(planeState: PlaneState) {
        planeX = planeState.planeX
        planeY = mHeight - planeState.planeHeight
        planeAngle = planeState.planeAngle
        dX = planeState.dX
        dY = planeState.dY
        up = planeState.up
        state = planeState.state
        pInterface = planeState.planeInterface
    }

    /// Private

    private fun drawPlane(c: Canvas) {
        val rotationMatrix = Matrix()
        rotationMatrix.postRotate(planeAngle)
        val rotatedPlane = Bitmap.createBitmap(plane, 0,0,plane.width, plane.height, rotationMatrix, true )
        val p = Paint()
        p.color = Color.WHITE
        c.drawBitmap(rotatedPlane, 0.0f, planeY.coerceAtLeast(mHeight * 0.2f), p)
        if(planeY >= mHeight - rotatedPlane.height && state == FLYING) {
            state = PAUSE
            pInterface?.onCrash()
        }
    }

    private fun drawGround(c : Canvas) {
        if(planeY >= -20) {
            val p = Paint()
            p.color = Color.WHITE
            p.strokeWidth = 3.0f
            val y =(mHeight - planeY - 50.0f).coerceAtLeast(mHeight - 20.0f)
            c.drawLine(0.0f, y, mWidth.toFloat(), y, p)
        }
    }

    private fun updatePlane() {
        dY += 0.005f * if(up) -1 else 1
        if(dY <= -1) up = false

        val newY = planeY + dY * (dY.absoluteValue * 10)

        planeAngle =  Math.toDegrees((sign(dY) * acos(dX / sqrt(dX.pow(2) + dY.pow(2)))).toDouble()).toFloat()
        planeY = newY
        planeX += dX
        pInterface?.updateDistance((planeX / 10).toInt())
    }

    interface PLaneInterface {
        fun onCrash()
        fun updateDistance(distance: Int)
    }
}