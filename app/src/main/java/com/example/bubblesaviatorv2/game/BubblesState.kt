package com.example.bubblesaviatorv2.game

import androidx.annotation.Keep

@Keep
data class BubblesState(val bubbles: List<Bubble>, val state: Int)
