package com.example.bubblesaviatorv2.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.example.bubblesaviatorv2.R
import com.example.bubblesaviatorv2.util.ColorFilters
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.random.Random

class BubblesView (context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var bubbleWidth = 0.0f

    private val bubble = BitmapFactory.decodeResource(context.resources, R.drawable.bubble)
    private var bubbles = List(6){ n -> Bubble(0,0, bubbleWidth, 0.0f, 0.0f, n + 1, false, ColorFilters.WHITE, Bubble.NORMAL) }

    private var bInterface: BubblesInterface? = null

    private var state = HIDDEN

    companion object {
        const val HIDDEN = 0
        const val SHOWN = 1
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            bubbleWidth = mWidth / 7.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            updateBubbles()
            drawBubbles(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> return true
            MotionEvent.ACTION_UP -> {
                if(state == SHOWN) checkPress(event.x, event.y)
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: BubblesInterface) {
        bInterface = i
    }

    fun restart() {
        state = SHOWN
        generateBubbles()
    }

    fun hide() {
        state = HIDDEN
    }

    fun getState() = BubblesState(bubbles, state)

    fun setState(bubblesState: BubblesState) {
        bubbles = bubblesState.bubbles
        state = bubblesState.state
    }

    /// Private

    private fun drawBubbles(c: Canvas) {
        if(state == SHOWN) {
            val p = Paint()
            p.textAlign = Paint.Align.CENTER
            p.textSize = bubbleWidth * 0.5f
            for(b in bubbles) {
                if(b.visible) {
                    p.colorFilter = b.colorFilter.filter
                    c.drawBitmap(bubble,null, Rect((b.x - b.size / 2.0f).toInt(), (b.y - b.size / 2.0f).toInt(), (b.x + b.size / 2.0f).toInt(), (b.y + b.size / 2.0f).toInt()), p)
                    if(b.state == Bubble.NORMAL) {
                        p.color = Color.parseColor("#476491")
                        c.drawText(b.number.toString(), b.x.toFloat(), b.y + b.size * 0.2f, p)
                    }
                }
            }
        }
    }

    private fun generateBubbles() {
        for(b in bubbles) {
            b.x = Random.nextInt((bubbleWidth / 2).toInt(), mWidth - (bubbleWidth / 2).toInt())
            b.y = Random.nextInt((bubbleWidth / 2).toInt(), mHeight - (bubbleWidth / 2).toInt())
            b.moveVectorX = Random.nextFloat() * Random.nextInt(-1, 1) * 5
            b.moveVectorY = Random.nextFloat() * Random.nextInt(-1, 1) * 5
            b.visible = true
            b.colorFilter = ColorFilters.WHITE
            b.state = Bubble.NORMAL
            b.size = bubbleWidth
        }
    }

    private fun updateBubbles() {
        var active = 0
        for(b in bubbles) {
            if(b.x + b.moveVectorX !in (b.size / 2)..(mWidth - b.size / 2)) b.moveVectorX *= -1
            if(b.y + b.moveVectorY !in (b.size / 2)..(mHeight - b.size / 2)) b.moveVectorY *= -1
            b.x += b.moveVectorX.toInt()
            b.y += b.moveVectorY.toInt()
            if(b.state != Bubble.NORMAL) {
                if(b.size > 5.0f) b.size--
                else b.visible = false
            }
            if(b.visible) active++
        }
        if(active == 0 && state == SHOWN) {
            var f = false
            for(b in bubbles) if(b.colorFilter == ColorFilters.RED) f = true
            if(f) bInterface?.onFail() else bInterface?.onSuccess()
            state = HIDDEN
        }
    }

    private fun checkPress(x: Float, y: Float) {
        for(b in bubbles) {
            if(b.state != Bubble.PRESSED) {
                if(x in (b.x - b.size / 2)..(b.x + b.size / 2) && y in (b.y - b.size / 2)..(b.y + b.size / 2)) {
                    b.state = Bubble.PRESSED
                    b.colorFilter = ColorFilters.GREEN
                    break
                } else {
                    for(bub in bubbles) {
                        if(bub.visible) {
                            bub.state = Bubble.PRESSED
                            bub.colorFilter = ColorFilters.RED
                        }
                    }
                    break
                }
            }
        }
    }

    interface BubblesInterface {
        fun onFail()
        fun onSuccess()
    }
}